package c4i.TemplateWebProject.Service;

import c4i.TemplateWebProject.Model.City;
import c4i.TemplateWebProject.Repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CityService {
    @Autowired
    CityRepository cityRepository;

    public City create(City city){
        return cityRepository.save(city);
    }
    public List<City> findAll(){
        List<City> cities = new ArrayList<>();
        cityRepository.findAll().forEach(cities::add);
        return cities;
    }
    public Optional<City> findById(int id){
        return cityRepository.findById((id));
    }
    public List<City> findByName(String name){
        List<City> cities = new ArrayList<>();
        return cityRepository.findByName(name);
    }
    public City update(int id, City city){
        if(this.findById(id).isPresent()){
            City newCity = this.findById(id).get();
            newCity.setName(city.getName());
            newCity.setPopulation(city.getPopulation());
            return cityRepository.save(newCity);
        }
        else
            return null;
    }
    public void deleteALl(){
        cityRepository.deleteAll();
    }
    public void deleteById(int id){
        cityRepository.deleteById(id);
    }
}
