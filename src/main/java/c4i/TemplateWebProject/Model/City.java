package c4i.TemplateWebProject.Model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;


@Entity
@Table(name = "cities")
@Getter
@Setter
@ToString
public class City {

    @Id
    @Column(name = "id")
    private int id ;
    @Column(name = "population")
    private int population ;
    @Column(name = "name")
    private String name;
}
