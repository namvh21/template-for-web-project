package c4i.TemplateWebProject.Repository;

import c4i.TemplateWebProject.Model.City;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface CityRepository extends CrudRepository<City,Long> {
    List<City> findByName(String name);
    Optional<City> findById(int id);

    @Transactional
    void deleteById(int id);
    @Transactional
    void deleteByName(String name);
}
