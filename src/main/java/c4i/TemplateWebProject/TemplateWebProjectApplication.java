package c4i.TemplateWebProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TemplateWebProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(TemplateWebProjectApplication.class, args);
	}

}
