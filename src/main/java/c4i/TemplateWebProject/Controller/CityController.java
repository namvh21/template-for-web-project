package c4i.TemplateWebProject.Controller;

import c4i.TemplateWebProject.Model.City;
import c4i.TemplateWebProject.Repository.CityRepository;
import c4i.TemplateWebProject.Service.CityService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@Tag(name="cities")
public class CityController {
    @Autowired
    CityService cityService;

    @Operation(description = "Xem danh sách City", responses = {
            @ApiResponse(content = @Content(array = @ArraySchema(schema = @Schema(implementation = City.class))), responseCode = "200") })
    @ApiResponses(value = {
            @ApiResponse(responseCode  = "200", description = "Thành công"),
            @ApiResponse(responseCode  = "401", description = "Chưa xác thực"),
            @ApiResponse(responseCode  = "403", description = "Truy cập bị cấm"),
            @ApiResponse(responseCode  = "404", description = "Không tìm thấy")
    })
    @CrossOrigin
    @PostMapping("/city")
    public ResponseEntity<City> apiCreate(@RequestBody City city)
    {
        try
        {
            System.out.println("Try to create a city");
            City newCity =  cityService.create(city);
            if (newCity != null){
                return new ResponseEntity<>(newCity, HttpStatus.CREATED);
            }
            else
                return new ResponseEntity<>(null,HttpStatus.BAD_REQUEST);

        }
        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @GetMapping("/city")
    public ResponseEntity<List<City>> apiFindALl(@RequestParam(required = false) String name){
        try{
            System.out.println("Try to get all cicies");
            List<City> cities = new ArrayList<>();
            if(name == null )
                cityService.findAll().forEach(cities::add);
            else
                cityService.findByName(name).forEach(cities::add);
            if(cities.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            else
                return new ResponseEntity<>(cities, HttpStatus.OK);
        }
        catch (Exception e ){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @GetMapping("/city/{id}")
    public ResponseEntity<City> apiFindById(@PathVariable int id)
    {
        try{
            System.out.println("Try to get an city with "+ id);
            Optional<City> city = cityService.findById(id);
            if(city.isPresent())
            {
                return new ResponseEntity<>(city.get(),HttpStatus.OK);
            }
            else
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);

        }
        catch (Exception e)
        {
            return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
        }
    }

    @CrossOrigin
    @PutMapping("/city/{id}")
    public ResponseEntity<City> apiUpdate(@PathVariable int id, @RequestBody City city)
    {
        System.out.print("Try to update an area ");
        City newCity = cityService.update(id,city);
        if (newCity == null)
        {
            System.out.println("but not found");
            return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<>(newCity,HttpStatus.OK);
        }

    }

    @CrossOrigin
    @DeleteMapping("/city/{id}")
    public ResponseEntity<City>  apiDeleteById(@PathVariable int id)
    {
        try{
            System.out.println("Try to delete city  :"+id);
            cityService.deleteById(id);
            return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
        }
        catch (Exception e )
        {
            System.out.println(e);
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @CrossOrigin
    @DeleteMapping("/city")
    public ResponseEntity<City> apiDeleteAll()
    {
        try
        {
            System.out.println("Try to delete all cities");
            cityService.deleteALl();
            return new ResponseEntity<>(null,HttpStatus.OK);
        }
        catch (Exception e)
        {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
